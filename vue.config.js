const {
  projectTitle,
  port
} = require('./src/common/config/baseConfig')
const path = require('path')
const CompressionWebpackPlugin = require('compression-webpack-plugin')
const resolve = dir => {
  return path.join(__dirname, dir)
}

const BASE_URL = process.env.NODE_ENV === 'production'
  ? './'
  : '/'
// 每次更改配置文件都需要重启
module.exports = {
  publicPath: BASE_URL,
  /* 代码保存时进行eslint检测 */
  lintOnSave: true,
  chainWebpack: config => {
    config.resolve.alias.set('@', resolve('src')) // key,value自行定义，比如.set('@@', resolve('src/components'))
      .set('_c', resolve('src/components'))
    config.plugin('html').tap(args => {
      args[0].title = projectTitle
      return args
    })
  },
  configureWebpack: config => {
    if (process.env.NODE_ENV === 'production') {
      config.plugins.push(new CompressionWebpackPlugin({
        algorithm: 'gzip',
        test: new RegExp('\\.(' + ['js', 'css'].join('|') + ')$'),
        threshold: 10240,
        minRatio: 0.8
      }))
    }
  },
  transpileDependencies: ['mathjs', 'sockjs-client'],
  /* 是否在构建生产包时生成 sourceMap 文件，false将提高构建速度 */
  productionSourceMap: false,
  /* 默认情况下，生成的静态资源在它们的文件名中包含了 hash 以便更好的控制缓存，你可以通过将这个选项设为 false 来关闭文件名哈希。(false的时候就是让原来的文件名不改变) */
  // filenameHashing: true,
  // 设置全局scss
  css: {
    loaderOptions: {
      sass: {
        prependData: ' @import "~@/assets/style/public.scss";',
        sassOptions: {
          outputStyle: 'expanded'
        }
      }
    }
  },
  devServer: {
    /* 是否自动打开浏览器 */
    open: true,
    host: '127.0.0.1',
    // host: '192.168.220.220',
    port: port,
    proxy: {
      '/api/': {
        ws: false,
        target: 'http://127.0.0.1:8888',
        // target: 'http://192.168.220.220:8888',
        changeOrigin: true,
        secure: false
      }
    }
  }
}
