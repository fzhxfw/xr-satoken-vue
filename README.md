### 一款基于SaToken轻量级Java权限认证框架构建的微服务后台开发脚手架对应的前端项目 XrSaTokenVue

本项目配合对应后端项目使用：

[XrSaTokenBoot](https://gitee.com/fzhxfw/xr-satoken-boot) （单体版本）

[XrSaTokenCloud](https://gitee.com/fzhxfw/xr-satoken-cloud) （微服务版本）

初始用户: weiyijian 初始密码: admin123456（已在登录页面表单中填充）

<p align="center">
 <img src="https://img.shields.io/badge/XrSaTokenCloud-v1.2.2-success.svg" alt="Build Status">
 <img src="https://img.shields.io/badge/SpringBoot-2.7.1-blue.svg" alt="Downloads">
 <img src="https://img.shields.io/badge/SpringCloud-2021.0.3-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/SpringCloudAlibaba-2021.0.1.0-blue.svg" alt="Coverage Status">
 <img src="https://img.shields.io/badge/Vue-2.6-blue.svg" alt="Downloads">
 <img src="https://img.shields.io/github/license/pig-mesh/pig"/>
</p>

## 系统说明

- 基于SpringBoot-2.7.1、SpringCloud-2021、SaToken-1.28.0 的 RBAC **权限管理系统**
- 基于Vue-2.6、ElementUI 实现系统后台页面的搭建
- 提供Docker、K8s等容器化部署技术支持
- 系统内置多种自定义注解，如日志注解@SystemLog，配合开启日志注解@EnableXrSaTokenCloudLog，切面处理实现跨模块的统一日志记录
- 内置基于Velocity模板构建的代码生成器，一键生成所需的单表操作前后端代码，如Excel导入、Excel导出、增删改查等
- 提供第三方分布式文件存储系统Minio的服务支持，支持大文件分片速传
- xr-dependencies模块中提供了各种主流文件存储服务的封装依赖包(如阿里云、腾讯云、七牛云、FastDFS、Minio（均不提供分片支持）)
  ，可按照开发者具体需求，在配置文件中编写
  对应的配置信息，启动类上使用@EnableAliyunFileService（开启阿里云文件存储的注解），其他文件服务注解类似，标准格式为@Enable____FileService，即可几行代码调用已封装
  好的接口实现对文件的各种操作
- 用户正在进行登录操作时，系统提供开启异步线程发送邮件通知到当前登录用户的电子邮箱的服务支持，默认false，可在配置文件中按需修改

### 核心依赖

| 依赖                 | 版本         |
|--------------------|------------|
| SpringBoot         | 2.7.1      |
| SpringCloud        | 2021.0.3   |
| SpringCloudAlibaba | 2021.0.1.0 |
| SaToken            | 1.28.0     |
| MybatisPlus        | 3.5.1      |
| Mysql              | 8.0.27     |
| HuTool             | 5.8.15     |

### 模块说明

```lua

###前端项目结构

xr - satoken - vue -- https://gitee.com/fzhxfw/xr-satoken-vue
        |-- src
|-- App.vue------------------------------页面入口文件
|-- main.js------------------------------项目启动脚本文件
|-- api------------------------------接口存放父目录
|-- system------------------------------系统相关接口存放子目录
|-- upload------------------------------文件分片上传相关接口存放子目录
|-- assets------------------------------静态文件存放父目录
|   |-- icon------------------------------系统图标存放子目录
|   |-- images------------------------------系统图片存放子目录
|-- login------------------------------登录页面相关图片存放二级子目录
|-- page------------------------------通知图片存放二级子目录
|-- system------------------------------404 NOT FOUND图片存放二级子目录
|   |-- style------------------------------element-ui全局scss样式存放目录                   
|   |-- tinyMce
|       |-- langs------------------------------国际化js文件存放目录        
|-- common------------------------------常用工具包存放父目录
|-- |-- components------------------------------组件全局注册文件存放子目录
|   |-- config------------------------------全局配置文件存放子目录
|   |-- constant------------------------------常量文件存放子目录
|   |-- directive------------------------------自定义指令文件存放子目录
|   |-- util------------------------------自定义工具包文件存放子目录
|   |-- validate------------------------------常用form表单校验文件存放子目录
|-- components------------------------------系统所有组件存放目录
|-- mock------------------------------测试样例存放目录
|-- router
|   |-- index.js------------------------------系统通用路由文件（重定向到404 NOT FOUND页面 或者 登录页面等）
|   |-- routers------------------------------路由配置存放目录           
|       |-- system.js------------------------------前端路由相关配置
|-- store------------------------------getter和setter文件存放目录
|-- views
|-- login------------------------------登录页面存放目录
|-- page
|-- system
|-- rightsManagement------------------------------权限管理页面目录
|-- roleManagement------------------------------角色管理页面目录
|-- userInfo------------------------------个人信息页面目录
|-- userManagement------------------------------用户管理页面目录
|-- dictionary------------------------------字典页面目录
|-- generator------------------------------代码生成器页面目录
|-- logManagement------------------------------日志管理页面目录
|-- student------------------------------学生示例模块页面目录
|-- systemSetting------------------------------系统设置页面目录
|-- teacher------------------------------教师示例模块页面目录
|-- redirect------------------------------重定向文件存放目录
|-- upload------------------------------文件分片上传页面存放目录
|-- 404.vue------------------------------404 NOT FOUND页面
|-- Home.vue------------------------------系统主页面
|-- Welcome.vue------------------------------系统欢迎页面

### 后端项目结构

xr-satoken-cloud -- https://gitee.com/fzhxfw/xr-satoken-cloud
├── xr-common -- 系统公共模块
└── xr-dependencies -- 系统封装依赖包
├── nacos-service-dependencies -- Nacos服务相关依赖包
├── aliyun-file-service-dependencies -- 阿里云文件存储服务依赖包
├── tencentcloud-file-service-dependencies -- 腾讯云文件存储服务依赖包
├── minio-file-service-dependencies -- Minio文件存储服务依赖包
├── qiniuyun-file-service-dependencies -- 七牛云文件存储服务依赖包
├── fastdfs-file-service-dependencies -- FastDFS文件存储服务依赖包
├── mail-service-dependencies -- 邮箱服务依赖包
└── shortmessage-service-dependencies -- 腾讯云短信服务依赖包
├── xr-model -- 对象实体存放模块（Pojo、Dto、Vo等）
├── xr-feign-api -- 远程调用接口存放模块
├── xr-nacos-service -- Nacos服务[8848]
├── xr-task-scheduling -- Xxl-Job分布式任务调度服务[8858]
├── xr-gateway -- 系统网关模块[8888]
├── xr-system -- 系统核心模块[40000]
├── xr-log -- 系统统一日志处理模块[40001]
├── xr-file-slice-upload -- 系统文件分片上传模块[40002]
├── xr-generator -- 系统代码生成器模块[40003]
└── xr-service -- 系统服务存放模块
└── service-student -- 学生服务示例[40004]
└── service-teacher -- 教师服务示例[40005]
```

### 项目功能演示

![](doc/images/1.png)       
![](doc/images/2.png)
![](doc/images/3.png)
![](doc/images/4.png)
![](doc/images/5.png)
![](doc/images/6.png)
![](doc/images/7.png)
![](doc/images/8.png)
![](doc/images/9.png)
![](doc/images/10.png)
![](doc/images/11.png)
![](doc/images/12.png)
![](doc/images/13.png)
![](doc/images/14.png)
![](doc/images/15.png)
![](doc/images/16.png)

### 本地开发运行

运行前端（需提前准备好NodeJs环境）:

1.克隆master分支稳定发行版代码到本地

git clone https://gitee.com/fzhxfw/xr-satoken-vue/tree/master/

2.使用淘宝镜像npm命令下载对应的依赖

npm install --registry=https://registry.npm.taobao.org

3.启动项目

npm run dev

运行后端：

克隆master分支稳定发行版代码到本地

git clone https://gitee.com/fzhxfw/xr-satoken-cloud/tree/master/

1.安装运行环境，RabbitMQ（√ 强制）、Redis（√ 强制）、Mysql（√ 强制）、Nacos（× 不强制，项目已集成，也可换成自己的）、Xxl-Job（×
不强制，项目已集成，也可换成自己的）

2.导入sql文件夹下的SQL脚本，完成数据库的初始化操作，每个SQL文件都对应其独立的数据库

3.本地启动Nacos 或者 启动项目中的Nacos模块（xr-nacos-service），一键导入nacos-config文件夹下的Nacos初始化包，修改对应配置信息

4.启动系统网关模块（xr-gateway）、系统核心模块(xr-system)、系统日志模块（xr-log）、系统文件分片上传模块(xr-file-slice-upload)
、系统代码生成器模块（xr-generator）、系统分布式任务调度模块（xr-task-scheduling）、
学生示例模块（service-student）、教师示例模块（service-teacher）

注意启动顺序：先启动xr-nacos-service，后启动xr-gateway，其他服务模块不分顺序。

### 开源协议

XrSaTokenCloud 开源软件遵循 [Apache 2.0 协议](https://www.apache.org/licenses/LICENSE-2.0.html)。
允许商业使用，但务必保留类作者、Copyright 信息。

### 其他说明

1. 欢迎提交 [PR](https://dwz.cn/2KURd5Vf)，注意对应提交对应 `dev` 分支
   代码规范 [spring-javaformat](https://github.com/spring-io/spring-javaformat)

   <details>
    <summary>代码规范说明</summary>

    1. 由于 <a href="https://github.com/spring-io/spring-javaformat" target="_blank">spring-javaformat</a>
       强制所有代码按照指定格式排版，未按此要求提交的代码将不能通过合并（打包）
    2. 如果使用 IntelliJ IDEA
       开发，请安装自动格式化软件 <a href="https://repo1.maven.org/maven2/io/spring/javaformat/spring-javaformat-intellij-idea-plugin/" target="_blank">
       spring-javaformat-intellij-idea-plugin</a>
    3. 其他开发工具，请参考 <a href="https://github.com/spring-io/spring-javaformat" target="_blank">
       spring-javaformat</a>
       说明，或`提交代码前`在项目根目录运行下列命令（需要开发者电脑支持`mvn`命令）进行代码格式化

    4. 父模块中引入spring-javaformat-maven-plugin格式化插件依赖
        ```
             <!--
                 代码格式插件，默认使用spring 规则，可运行命令进行项目格式化：./mvnw spring-javaformat:apply 或 mvn spring-javaformat:apply，可在IDEA中安装插件        
                 以下插件进行自动格式化：
                 https://repo1.maven.org/maven2/io/spring/javaformat/spring-javaformat-intellij-idea-plugin
             -->
             <plugin>
                 <groupId>io.spring.javaformat</groupId>
                 <artifactId>spring-javaformat-maven-plugin</artifactId>
                 <version>0.0.10</version>
                 <executions>
                     <execution>
                         <phase>validate</phase>
                         <inherited>true</inherited>
                         <goals>
                             <goal>validate</goal>
                         </goals>
                     </execution>
                 </executions>
             </plugin>
        ```
   终端键入命令： mvn spring-javaformat:apply

   </details>

2. 欢迎提交 [issue](https://gitee.com/fzhxfw/xr-satoken-cloud/issues)，请写清楚遇到问题的原因、开发环境、复显步骤。

3. 联系作者 <a href="mailto:e_341877120_mail@163.com">e_341877120_mail@163.com</a>
