import Mock from 'mockjs'
import demo from './data/demo'
// import menu from './data/menu'
import login from './data/login'
// 配置Ajax请求延时，可用来测试网络延迟大时项目中一些效果
Mock.setup({
  timeout: 1000
})

Mock.mock(/\/mock-getDemo/, demo.getData())

// Mock.mock(/\/api\/v1\/account\/menu/, menu.getMenu())

Mock.mock(/\/mock-login/, login.getLogin())

Mock.mock(/\/mock-userInfo/, login.getInfo())
export default Mock
