export default {
  getMenu: () => ({
    msg: '获取成功！',
    data: [
      {
        name: '系统管理',
        index: '1',
        children: [
          {
            name: '账号管理',
            index: '1-1',
            children: [
              {
                name: '新增数据',
                path: '/userManagementAdd',
                index: '1-1-1'
              }, {
                name: '查看数据',
                path: '/userManagementList',
                index: '1-1-2'
              }
            ]
          },
          {
            name: '角色管理',
            index: '1-2',
            children: [
              {
                name: '新增数据',
                path: '/roleManagementAdd',
                index: '1-2-1'
              }, {
                name: '查看数据',
                path: '/roleManagementList',
                index: '1-2-2'
              }
            ]
          },
          {
            name: '菜单权限管理',
            index: '1-3',
            path: '/rightsManagement'
          }
        ]
      }
    ],
    code: 10000
  })
}
