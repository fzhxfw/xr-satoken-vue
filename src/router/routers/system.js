export default [
  {
    path: '/',
    name: 'welcome',
    component: () => import('@/views/Welcome'),
    meta: {
      name: '欢迎使用'
    }
  },
  {
    path: '/dictionary',
    name: 'dictionary',
    component: () => import('@/views/page/system/dictionary/listPage'),
    meta: {
      name: '字典管理',
      keepAlive: true
    }
  },
  {
    path: '/userManagementList',
    name: 'userManagementList',
    component: () => import('@/views/page/system/userManagement/listPage'),
    meta: {
      name: '账号列表',
      keepAlive: true
    }
  },
  {
    path: '/userManagementAdd',
    name: 'userManagementAdd',
    component: () => import('@/views/page/system/userManagement/addPage'),
    meta: {
      name: '新增账号',
      keepAlive: true
    }
  },
  {
    path: '/roleManagementList',
    name: 'roleManagementList',
    component: () => import('@/views/page/system/roleManagement/listPage'),
    meta: {
      name: '角色列表',
      keepAlive: true
    }
  },
  {
    path: '/roleManagementAdd',
    name: 'roleManagementAdd',
    component: () => import('@/views/page/system/roleManagement/addPage'),
    meta: {
      name: '新增角色',
      keepAlive: true
    }
  },
  {
    path: '/rightsManagement',
    name: 'rightsManagement',
    component: () => import('@/views/page/system/rightsManagement/listPage'),
    meta: {
      name: '菜单权限管理',
      keepAlive: true
    }
  },
  {
    path: '/operationLogManagement',
    name: 'operationLogManagement',
    component: () => import('@/views/page/system/logManagement/operationLogList.vue'),
    meta: {
      name: '操作日志管理',
      keepAlive: true
    }
  },
  {
    path: '/exceptionLogManagement',
    name: 'exceptionLogManagement',
    component: () => import('@/views/page/system/logManagement/exceptionLogList.vue'),
    meta: {
      name: '异常日志管理',
      keepAlive: true
    }
  },
  {
    path: '/sysSetting',
    name: 'sysSetting',
    component: () => import('@/views/page/system/systemSetting/addPage'),
    meta: {
      name: '系统设置',
      keepAlive: true
    }
  },
  {
    path: '/generator',
    name: 'generator',
    component: () => import('@/views/page/system/generator/generatorList'),
    meta: {
      name: '代码生成器',
      keepAlive: true
    }
  },
  {
    path: '/minio-slice-upload',
    name: 'minio-slice-upload',
    component: () => import('@/views/upload/minio-slice-upload'),
    meta: {
      name: '文件上传',
      keepAlive: true
    }
  },
  {
    path: '/fileList',
    name: 'fileList',
    component: () => import('@/views/upload/fileList'),
    meta: {
      name: '文件列表',
      keepAlive: true
    }
  },
  {
    path: '/userInfo',
    name: 'userInfo',
    meta: {
      name: '个人信息'
    },
    component: () => import('@/views/page/system/userInfo/index')
  },
  {
    path: '/student',
    name: 'student',
    meta: {
      name: '学生信息管理',
      keepAlive: true
    },
    component: () => import('@/views/page/system/student/studentList')
  },
  {
    path: '/teacher',
    name: 'teacher',
    meta: {
      name: '教师信息管理',
      keepAlive: true
    },
    component: () => import('@/views/page/system/teacher/teacherList')
  }
]
