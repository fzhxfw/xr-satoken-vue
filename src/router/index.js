import Vue from 'vue'
import VueRouter from 'vue-router'

const modulesFiles = require.context('./routers', true, /\.js$/)
const modules = modulesFiles.keys().reduce((modules, modulePath) => {
  const value = modulesFiles(modulePath)
  modules = modules.concat(value.default)
  return modules
}, [])

Vue.use(VueRouter)
/**
 * 如果含有子路由, 父路由请勿添加name属性
 */
const routes = [
  {
    path: '/',
    component: () => import('@/views/Home'),
    children: [...modules]
  },
  {
    path: '/redirect',
    component: () => import('@/views/Home'),
    hidden: true,
    children: [
      {
        name: 'redirect',
        path: '/redirect/:path*',
        component: () => import('@/views/redirect/index')
      }
    ]
  },
  {
    path: '/login',
    name: 'log',
    component: () => import('@/views/login')
  },
  {
    path: '*',
    name: 'notFind',
    component: () => import('@/views/404')
  }
]

const router = new VueRouter({
  base: process.env.BASE_URL,
  routes
})

export default router
