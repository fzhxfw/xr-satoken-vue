import precautions from './page/precautions'
import centerForm from './page/centerForm'
import tableEncapsulation from '@/components/tableEncapsulation'

export default {
  precautions,
  tableEncapsulation,
  centerForm
}
