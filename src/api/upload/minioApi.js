import axiosExtra from 'axios-extra'
import httpService from '@/common/config/httpService'

const httpExtra = axiosExtra.create({
  maxConcurrent: 5, // 并发为5
  queueOptions: {
    retry: 3, // 请求失败时，最多会重试3次
    retryIsJump: false // 是否立即重试，否则将在请求队列尾部插入重试请求
  }
})

/**
 * 根据文件的md5获取未上传完的任务
 * @param identifier 文件md5
 * @returns {Promise<AxiosResponse<any>>}
 */
const taskInfo = (identifier) => {
  return httpService.get(`/api/v1/file/tasks/${identifier}`)
}

/**
 * 初始化一个分片上传任务
 * @param identifier 文件md5
 * @param fileName 文件名称
 * @param totalSize 文件大小
 * @param chunkSize 分块大小
 * @returns {Promise<AxiosResponse<any>>}
 */
const initTask = ({
  identifier,
  fileName,
  totalSize,
  chunkSize
}) => {
  return httpService.post('/api/v1/file/tasks', {
    identifier,
    fileName,
    totalSize,
    chunkSize
  })
}

/**
 * 获取预签名分片上传地址
 * @param identifier 文件md5
 * @param partNumber 分片编号
 * @returns {Promise<AxiosResponse<any>>}
 */
const preSignUrl = ({
  identifier,
  partNumber
}) => {
  return httpService.get(`/api/v1/file/tasks/${identifier}/${partNumber}`)
}

/**
 * 合并分片
 * @param identifier
 * @returns {Promise<AxiosResponse<any>>}
 */
const merge = (identifier) => {
  return httpService.post(`/api/v1/file/tasks/merge/${identifier}`)
}

export {
  taskInfo,
  initTask,
  preSignUrl,
  merge,
  httpExtra
}
