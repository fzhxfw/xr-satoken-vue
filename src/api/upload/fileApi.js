import httpService from '@/common/config/httpService'

export default {

    async selectPageOfFile (current, size, params) {
        return await httpService.get(`/api/v1/file/selectPage/${current}/${size}`, params)
    },
    async selectOneOfFile (id, params) {
        return await httpService.get(`/api/v1/file/selectOneById/${id}`, params)
    },
    async insertOfFile (params) {
        return await httpService.post('/api/v1/file/insert', params)
    },
    async updateOfFile (params) {
        return await httpService.put('/api/v1/file/update', params)
    },
    async deleteOfFile (id, params) {
        return await httpService.delete(`/api/v1/file/delete/${id}`, params)
    },
    async deleteBatchOfFile (params) {
        return await httpService.post('/api/v1/file/deleteBatch', params)
    }

}
