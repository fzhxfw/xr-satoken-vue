import axios from '@/common/config/axiosConfig'

export const userLogin = (data) => {
  return axios.post('/api/v1/login/login', data)
}

export const userLogout = (data) => {
  return axios.request({
    url: '/api/v1/login/signOut',
    params: data,
    method: 'get'
  })
}

export const userInfo = (data) => {
  return axios.request({
    url: '/api/v1/account/userInfo',
    params: data,
    method: 'get'
  })
}
export const permissions = (data) => {
  return axios.request({
    url: '/api/v1/login/permission',
    params: data,
    method: 'get'
  })
}
export const btnPermissions = () => {
  return axios.request({
    url: '/api/v1/login/btnPermission',
    method: 'get'
  })
}
export const getUserMenu = (data) => {
  return axios.request({
    url: '/api/v1/account/menu',
    params: data,
    method: 'get'
  })
}
