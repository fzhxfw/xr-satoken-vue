import axios from '@/common/config/axiosConfig'

export const uploadFileForEditorPic = (data) => {
  return axios.post('/api/v1/richText/uploadRichTextPic', data, {
    headers: {
      'Content-Type': 'multipart/form-data'
    }
  })
}
