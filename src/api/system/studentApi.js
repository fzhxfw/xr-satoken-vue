import httpService from '@/common/config/httpService'

export default {

  async selectPageOfStudent (current, size, params) {
    return await httpService.get(`/api/v1/student/selectPage/${current}/${size}`, params)
  },
  async selectOneOfStudent (id, params) {
    return await httpService.get(`/api/v1/student/selectOneById/${id}`, params)
  },
  async insertOfStudent (params) {
    return await httpService.post('/api/v1/student/insert', params)
  },
  async updateOfStudent (params) {
    return await httpService.put('/api/v1/student/update', params)
  },
  async deleteOfStudent (id, params) {
    return await httpService.delete(`/api/v1/student/delete/${id}`, params)
  },
  async deleteBatchOfStudent (params) {
    return await httpService.post('/api/v1/student/deleteBatch', params)
  }

}
