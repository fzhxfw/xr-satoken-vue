import httpService from '@/common/config/httpService'

export default {

  async selectPageOfTeacher (current, size, params) {
    return await httpService.get(`/api/v1/teacher/selectPage/${current}/${size}`, params)
  },
  async selectOneOfTeacher (id, params) {
    return await httpService.get(`/api/v1/teacher/selectOneById/${id}`, params)
  },
  async insertOfTeacher (params) {
    return await httpService.post('/api/v1/teacher/insert', params)
  },
  async updateOfTeacher (params) {
    return await httpService.put('/api/v1/teacher/update', params)
  },
  async deleteOfTeacher (id, params) {
    return await httpService.delete(`/api/v1/teacher/delete/${id}`, params)
  },
  async deleteBatchOfTeacher (params) {
    return await httpService.post('/api/v1/teacher/deleteBatch', params)
  }

}
