import httpService from '@/common/config/httpService'

export default {

  async selectPageOfGenerator (current, size, params) {
    return await httpService.get(`/api/v1/generator/selectPage?current=${current}&size=${size}`, params)
  }

}
