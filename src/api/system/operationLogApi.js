import httpService from '@/common/config/httpService'

export default {

    async selectPageOfOperationLog (current, size, params) {
        return await httpService.get(`/api/v1/log/operationLog/selectPage/${current}/${size}`, params)
    },
    async selectOneOfOperationLog (id, params) {
        return await httpService.get(`/api/v1/log/operationLog/selectOneById/${id}`, params)
    },
    async insertOfOperationLog (params) {
        return await httpService.post('/api/v1/log/operationLog/insert', params)
    },
    async updateOfOperationLog (params) {
        return await httpService.put('/api/v1/log/operationLog/update', params)
    },
    async deleteOfOperationLog (id, params) {
        return await httpService.delete(`/api/v1/log/operationLog/delete/${id}`, params)
    },
    async deleteBatchOfOperationLog (params) {
        return await httpService.post('/api/v1/log/operationLog/deleteBatch', params)
    }

}
