import httpService from '@/common/config/httpService'

export default {

    async selectPageOfExceptionLog (current, size, params) {
        return await httpService.get(`/api/v1/log/exceptionLog/selectPage/${current}/${size}`, params)
    },
    async selectOneOfExceptionLog (id, params) {
        return await httpService.get(`/api/v1/log/exceptionLog/selectOneById/${id}`, params)
    },
    async insertOfExceptionLog (params) {
        return await httpService.post('/api/v1/log/exceptionLog/insert', params)
    },
    async updateOfExceptionLog (params) {
        return await httpService.put('/api/v1/log/exceptionLog/update', params)
    },
    async deleteOfExceptionLog (id, params) {
        return await httpService.delete(`/api/v1/log/exceptionLog/delete/${id}`, params)
    },
    async deleteBatchOfExceptionLog (params) {
        return await httpService.post('/api/v1/log/exceptionLog/deleteBatch', params)
    }

}
