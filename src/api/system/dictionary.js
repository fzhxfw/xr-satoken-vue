import axios from '@/common/config/axiosConfig'
import qs from 'qs'

export const queryPageList = (params) => {
  return axios.get('/api/v1/dictionary/queryByPage', {
    params,
    paramsSerializer: function (data) {
      return qs.stringify(data)
    }
  })
}

export const listDicByParentCode = (code) => {
  return axios.get('/api/v1/dictionary/listDicByParentCode', {
    params: { code }
  })
}
export const selectOne = (id) => {
  return axios.get(`/api/v1/dictionary/queryOne/${id}`)
}
export const insert = (data) => {
  return axios.post('/api/v1/dictionary/insert', data)
}
export const update = (data) => {
  return axios.put('/api/v1/dictionary/update', data)
}
export const deleteById = (id) => {
  return axios.delete(`/api/v1/dictionary/delete/${id}`)
}
