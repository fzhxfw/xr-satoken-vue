import axios from '@/common/config/axiosConfig'

/*
* 获取权限列表
* */
export const getRightsList = () => {
  return axios.get('/api/v1/permission/permissionTree')
}

export const permissionByUserId = (id) => {
  return axios.get('/api/v1/permission/permissionByUserId', {
    params: { id }
  })
}
export const selectOne = (id) => {
  return axios.get(`/api/v1/permission/queryOne/${id}`)
}
export const insert = (data) => {
  return axios.post('/api/v1/permission/insert', data)
}
export const update = (data) => {
  return axios.put('/api/v1/permission/update', data)
}
export const deleteById = (id) => {
  return axios.delete(`/api/v1/permission/delete/${id}`)
}
