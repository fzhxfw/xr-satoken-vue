import axios from '@/common/config/axiosConfig'
import qs from 'qs'
export const queryOperationLog = (params) => {
  return axios.get('/api/v1/operationLog/queryByPage', {
    params,
    paramsSerializer: function (data) {
      return qs.stringify(data)
    }
  })
}
export const selectOneForOperationLog = (id) => {
  return axios.get(`/api/v1/operationLog/queryOne/${id}`)
}
export const queryExceptionLog = (params) => {
  return axios.get('/api/v1/exceptionLog/queryByPage', {
    params,
    paramsSerializer: function (data) {
      return qs.stringify(data)
    }
  })
}
export const selectOneForExceptionLog = (id) => {
  return axios.get(`/api/v1/exceptionLog/queryOne/${id}`)
}
