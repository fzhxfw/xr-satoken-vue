import axios from '@/common/config/axiosConfig'
import qs from 'qs'

export const insert = (data) => {
  return axios.post('/api/v1/account/insert', data)
}
export const update = (data) => {
  return axios.put('/api/v1/account/update', data)
}
export const associatedRole = (data) => {
  return axios.post('/api/v1/account/associatedRole', data)
}
export const queryPageList = (params) => {
  return axios.get('/api/v1/account/queryByPage', {
    params,
    paramsSerializer: function (data) {
      return qs.stringify(data)
    }
  })
}
export const listInitAccount = () => {
  return axios.get('/api/v1/account/listInitAccount')
}
export const queryAll = () => {
  return axios.get('/api/v1/account/queryAll')
}
export const selectOne = (id) => {
  return axios.get(`/api/v1/account/queryOne/${id}`)
}
export const deleteById = (id) => {
  return axios.delete(`/api/v1/account/delete/${id}`)
}
export const batchDeletionByIdList = (idList) => {
  return axios.delete('/api/v1/account/batchDelete', {
    params: { idList },
    paramsSerializer: function (data) {
      return qs.stringify(data, { arrayFormat: 'repeat' })
    }
  })
}
export const disOrEnaAccount = (accountId) => {
  return axios.put(`/api/v1/account/disOrEnaAccount?accountId=${accountId}`)
}
export const setInitAccount = (idList) => {
  return axios.put('/api/v1/account/setInitAccount', idList)
}
