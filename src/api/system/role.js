import axios from '@/common/config/axiosConfig'
import qs from 'qs'

export const listCheckedPermissionForRole = (id) => {
  return axios.get('/api/v1/role/listCheckedPermissionForRole', { params: { id } })
}
export const listInitRole = () => {
  return axios.get('/api/v1/role/listInitRole')
}
export const listDefaultRole = () => {
  return axios.get('/api/v1/role/listDefaultRole')
}
export const queryAll = () => {
  return axios.get('/api/v1/role/queryAll')
}

export const roleByUserId = (id) => {
  return axios.get('/api/v1/role/roleByUserId', {
    params: { id }
  })
}
export const assignmentPermissionForRole = (data) => {
  return axios.post('/api/v1/role/assignmentPermissionForRole', data)
}
export const insert = (data) => {
  return axios.post('/api/v1/role/insert', data)
}
export const update = (data) => {
  return axios.put('/api/v1/role/update', data)
}
export const associatedRole = (data) => {
  return axios.post('/api/v1/role/associatedRole', data)
}
export const queryPageList = (params) => {
  return axios.get('/api/v1/role/queryByPage', {
    params,
    paramsSerializer: function (data) {
      return qs.stringify(data)
    }
  })
}
export const selectOne = (id) => {
  return axios.get(`/api/v1/role/queryOne/${id}`)
}
export const deleteById = (id) => {
  return axios.delete(`/api/v1/role/delete/${id}`)
}
export const setInitRole = (idList) => {
  return axios.put('/api/v1/role/setInitRole', idList)
}
export const setDefaultRole = (idList) => {
  return axios.put('/api/v1/role/setDefaultRole', idList)
}
export const batchDeletionByIdList = (idList) => {
  return axios.delete('/api/v1/role/batchDelete', {
    params: { idList },
    paramsSerializer: function (data) {
      return qs.stringify(data, { arrayFormat: 'repeat' })
    }
  })
}
export const disOrEnaAccount = (accountId) => {
  return axios.put(`/api/v1/role/disOrEnaAccount?roleId=${accountId}`)
}
