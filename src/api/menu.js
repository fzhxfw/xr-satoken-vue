import axios from '@/common/config/axiosConfig'

export const getMenu = data => {
  return axios.request({
    url: '/mock/getMenu',
    data: data,
    type: 'get'
  })
}
