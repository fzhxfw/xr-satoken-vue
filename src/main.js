import 'babel-polyfill'
import 'es6-promise/auto'
import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import './common/config/routerConfig' // 路由全局配置
import './common/config/UIConfig' // UI全局配置
import './common/directive' // 全局指令
import './common/components'
import VueClipboard from 'vue-clipboard2'
import Router from 'vue-router'
import utils from '@/common/util/utils'
import dayjs from 'dayjs'
import './mock'

// 引入Constant下所有js文件
const modules = require.context('./common/constant', true, /\.js$/)
let constant = {}
modules.keys().forEach(key => {
  const mod = modules(key)
  const content = mod.__esModule && mod.default ? mod.default : mod
  constant = { ...constant, ...content }
})

Vue.prototype.$dayjs = dayjs
Vue.prototype.$constant = constant
Vue.prototype.$utils = utils

const routerPush = Router.prototype.push
Router.prototype.push = function push (location) {
  return routerPush.call(this, location).catch(error => error)
}
Vue.config.productionTip = false
Vue.use(VueClipboard)

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
