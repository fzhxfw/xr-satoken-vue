import Vue from 'vue'
// 注册全局组件
import components from '@/components'

Object.keys(components).forEach(key => {
  Vue.component(`v-${key}`, components[key])
})
