import Vue from 'vue'

/**
 * 权限指令
 * @type {DirectiveOptions}
 */
const has = Vue.directive('has', {
  inserted: function (el, binding, vnode) {
    // 获取页面按钮权限
    const btnPermissions = binding.value
    // console.log(btnPermissions)
    if (!hasCheck(btnPermissions, vnode.context)) {
      el.remove()
    }
  }
})
// 权限检查方法
const hasCheck = function (value, vm) {
  // 获取用户按钮权限
  const btnPermissionsArr = vm.$store.getters.btnPermissions
  // console.log(btnPermissionsArr)
  if (!btnPermissionsArr || btnPermissionsArr.length === 0) {
    return false
  }
  return btnPermissionsArr.includes(value)
}
export default has
