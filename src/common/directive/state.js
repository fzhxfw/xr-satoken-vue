import Vue from 'vue'
import { colorPrimary, colorDanger } from '@/assets/style/public.scss'

export const state = Vue.directive(
  'state',
  {
    inserted: function (el, binding, vnode) {
      const value = binding.value
      let color = colorPrimary
      switch (value) {
        case '停用':
          color = colorDanger
      }
      el.style.color = color
    }
  }
)
export const judge = Vue.directive(
  'judge',
  {
    inserted: function (el, binding, vnode) {
      const value = binding.value
      if (value) {
        el.className = 'el-icon-check'
        el.style.fontSize = '16px'
      }
    }
  }
)
export default {
  state,
  judge
}
