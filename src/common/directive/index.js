import has from './btnPermissions'
import checkRole from './checkRole'
import dialogDrag from './dialogDrag'
// 状态 和 判断
import { state, judge } from './state'

export default {
  has,
  dialogDrag,
  checkRole,
  state,
  judge
}
