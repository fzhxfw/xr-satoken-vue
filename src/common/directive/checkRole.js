import Vue from 'vue'

/**
 * 权限指令
 * @type {DirectiveOptions}
 */
const checkRole = Vue.directive('checkRole', {
  inserted: function (el, binding, vnode) {
    // 获取页面按钮权限
    const roleCode = binding.value
    console.log(roleCode)
    if (!hasCheck(roleCode, vnode.context)) {
      el.remove()
    }
  }
})
// 权限检查方法
const hasCheck = function (value, vm) {
  // 获取用户按钮权限
  const roleArr = vm.$store.getters.permissions
  console.log(roleArr)
  if (!roleArr || roleArr.length === 0) {
    return false
  }
  return roleArr.includes(value)
}
export default checkRole
