const $math = require('mathjs')
function comp (_func, args) {
  let t = $math.chain($math.bignumber(args[0]))
  for (let i = 1; i < args.length; i++) {
    t = t[_func]($math.bignumber(args[i]))
  }
  // 防止超过6位使用科学计数法
  return parseFloat(t.done())
}
export default {
  add () {
    return comp('add', arguments)
  },
  subtract () {
    return comp('subtract', arguments)
  },
  multiply () {
    return comp('multiply', arguments)
  },
  divide () {
    return comp('divide', arguments)
  },
  secondsToStr (temp) {
    const years = Math.floor(temp / 31536000)
    if (years) {
      return years + ' year' + numberEnding(years)
    }
    const days = Math.floor((temp %= 31536000) / 86400)
    if (days) {
      return days + ' day' + numberEnding(days)
    }
    const hours = Math.floor((temp %= 86400) / 3600)
    if (hours) {
      return hours + ' hour' + numberEnding(hours)
    }
    const minutes = Math.floor((temp %= 3600) / 60)
    if (minutes) {
      return minutes + ' minute' + numberEnding(minutes)
    }
    const seconds = temp % 60
    return seconds + ' second' + numberEnding(seconds)

    function numberEnding (number) {
      return (number > 1) ? 's' : ''
    }
  },
  /**
   *可用于页面 面包屑 点击子菜单 查询父菜单
   *树形数组结构 通过子节点 获取 其所有父节点
   *第一个参数 树形结构数据 ,第二个参数 子节点id,第三个参数 递归时 携带的生成的数组
   */
  findIndexArray (data, id, indexArray) {
    const arr = Array.from(indexArray)
    for (let i = 0, len = data.length; i < len; i++) {
      arr.push(data[i])
      if (data[i].path === id) {
        return arr
      }
      const children = data[i].children
      if (children && children.length) {
        const result = this.findIndexArray(children, id, arr)
        if (result) return result
      }
      arr.pop()
    }
    return false
  },
  /**
   * url 添加参数
   * @param url 被添加的url
   * @param name 参数名
   * @param value 参数值
   * @returns {string|*}
   */
  addUrlParam (url, name, value) {
    const urlArr = url.split('#')
    let currentUrl = urlArr[0]
    if (/\?/g.test(currentUrl)) {
      if (/name=[-\w]{4,25}/g.test(currentUrl)) {
        currentUrl = currentUrl.replace(/name=[-\w]{4,25}/g, name + '=' + value)
      } else {
        currentUrl += '&' + name + '=' + value
      }
    } else {
      currentUrl += '?' + name + '=' + value
    }
    if (urlArr[1]) {
      return currentUrl + '#' + urlArr[1]
    } else {
      return currentUrl
    }
  }
}
