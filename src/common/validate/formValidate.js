/**
 * 电话校验
 * @param rule
 * @param value
 * @param callback
 * @returns {*}
 */
export const validPhone = (rule, value, callback) => {
  if (value === '') {
    callback(new Error('请输入电话号码'))
  } else {
    const mobile = /^1([3456789])\d{9}$/ // 最新16手机正则
    if (!mobile.test(value)) {
      return callback(
        new Error('请输入正确的电话号码!')
      )
    }
    callback()
  }
}
/**
 * 账户或者密码校验
 * @param rule
 * @param value
 * @param callback
 */
export const validText = (rule, value, callback) => {
  const contains_uppercase = /^(?!\d+$)[\da-zA-Z]{8,16}$/.test(value) // 大写
  if (contains_uppercase) {
    callback()
  } else {
    callback(new Error('填写内容必须为8-16大小写字母或数字(不能是纯数字)!'))
  }
}

/**
 * 第二代身份证校验
 * @param rule
 * @param value
 * @param callback
 */
export const validIdCard = (rule, value, callback) => {
  const contains_uppercase = /^[1-9]\d{5}(18|19|([23]\d))\d{2}((0[1-9])|(10|11|12))(([0-2][1-9])|10|20|30|31)\d{3}[0-9Xx]$/.test(value) // 大写
  if (contains_uppercase) {
    callback()
  } else {
    callback(new Error('您输入的身份证号码不是有效格式!'))
  }
}
/**
 * 全大写字母校验
 * @param rule
 * @param value
 * @param callback
 */
export const validUppercaseLetter = (rule, value, callback) => {
  const contains_uppercase = /^[A-Z]*$/.test(value) // 大写
  if (contains_uppercase) {
    callback()
  } else {
    callback(new Error('填写内容必须为大写字母'))
  }
}
