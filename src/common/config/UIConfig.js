import Vue from 'vue'
import ElementUI from 'element-ui'
import '@/assets/icon/iconfont.css'
import '@/assets/style/element-variables.scss'

// 弹出框 设置不可点击空白关闭
ElementUI.Dialog.props.closeOnClickModal.default = false
ElementUI.Form.props.labelSuffix.default = ' : '
ElementUI.Input.props.clearable.default = true

ElementUI.Select.props.clearable = { type: Boolean, default: true }
ElementUI.TimePicker.mixins[0].props.clearable.default = true
Vue.use(ElementUI, {
  size: 'small',
  zIndex: 3000
})
