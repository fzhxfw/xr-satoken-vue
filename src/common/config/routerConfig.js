// import Vue from 'vue'
import router from '../../router' // 路由
import store from '../../store'// vuex
import { Message } from 'element-ui'
// 路由钩子
router.beforeEach(async (to, from, next) => {
  if (to.name === 'notFind') {
    next()
    return
  }
  const hasToken = store.getters.token
  if (hasToken) {
    if (to.path === '/login') {
      next({ path: '/' })
    } else if (to.path === '/' || to.name === 'redirect') {
      next()
    } else {
      let permissions = store.getters.permissions
      const isHasPermission = permissions && permissions.length > 0
      if (!isHasPermission) {
        permissions = await store.dispatch('user/getPermission')
      }
      if (permissions.find(item => item === to.path)) {
        next()
      } else {
        Message({
          message: '当前用户无权限访问',
          type: 'error',
          duration: 5 * 1000
        })
      }
    }
  } else {
    if (to.path !== '/login') {
      next('/login')
    } else {
      next()
    }
  }
})
