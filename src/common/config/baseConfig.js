module.exports = {
  // axios请求的baseUrl，用指定服务器接口url前缀
  axiosBaseUrl: process.env.NODE_ENV === 'development' ? '/xr-satoken-vue' : '/xr-satoken-vue',
  // 浏览器标签上和登录页面以及页面左上角显示的名称
  projectTitle: 'xr-satoken-vue',
  // 开发阶段端口
  port: 30000
}
