const state = {
  pageTabs: [],
  isHeaderMenu: false
}

const mutations = {
  SET_IS_HEADER_Menu: (state, isHeaderMenu) => {
    state.isHeaderMenu = isHeaderMenu
  },
  SET_PAGE_TABS: (state, pageTabs) => {
    state.pageTabs = pageTabs
  },
  REMOVE_PAGE_TABS: (state) => {
    state.userInfo = []
  }
}

const actions = {}

export default {
  namespaced: true,
  state,
  mutations,
  actions
}
// 登录：当用户填写完账号和密码后向服务端验证是否正确，验证通过之后，服务端会返回一个token，拿到token之后（我会将这个token存贮到cookie中，保证刷新页面后能记住用户登录状态），前端会根据token再去拉取一个 user_info 的接口来获取用户的详细信息（如用户权限，用户名等等信息）。
// 权限验证：通过token获取用户对应的 role，动态根据用户的 role 算出其对应有权限的路由，通过 router.addRoutes 动态挂载这些路由。
