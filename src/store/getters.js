const getters = {
  token: state => state.user.token, // token
  userInfo: state => state.user.userInfo, // 用户名
  permissions: state => state.user.permissions,
  btnPermissions: state => state.user.btnPermissions,
  menu: state => state.user.menu,
  isHeaderMenu: state => state.system.isHeaderMenu,
  pageTabs: state => state.system.pageTabs
}
export default getters
